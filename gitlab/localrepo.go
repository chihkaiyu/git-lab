package gitlab

import (
	"fmt"

	"gitlab.com/jramsay/git-lab/config"
	"gitlab.com/jramsay/git-lab/git"
)

type GitRepo struct {
	remotes []Remote
}

func LocalRepo() (repo *GitRepo, err error) {
	repo = &GitRepo{}

	_, err = git.Dir()
	if err != nil {
		err = fmt.Errorf("fatal: Not a git repository")
		return
	}

	return
}

func (r *GitRepo) DefaultProject() (project *Project, err error) {
	remote, err := r.DefaultRemote()
	if err != nil {
		return
	}

	project, err = remote.Project()
	return
}

func (repo *GitRepo) DefaultRemote() (remote *Remote, err error) {
	repo.loadRemotes()

	for _, r := range repo.remotes {
		if config.GetServer(r.FetchURL.Hostname()) != nil {
			remote = &r
		} else if r.FetchURL != r.PushURL && config.GetServer(r.PushURL.Hostname()) != nil {
			remote = &r
		}
	}

	if remote == nil {
		err = fmt.Errorf("Can't find configured git remote origin")
	}

	return
}

func (r *GitRepo) CurrentBranch() (*Branch, error) {
	head, err := git.CurrentBranch()
	if err != nil {
		return nil, err
	}

	return &Branch{r, head}, nil
}

func (r *GitRepo) loadRemotes() error {
	if r.remotes != nil {
		return nil
	}

	remotes, err := Remotes()
	if err != nil {
		return err
	}
	r.remotes = remotes

	return nil
}
