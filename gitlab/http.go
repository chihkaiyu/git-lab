package gitlab

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

type simpleClient struct {
	httpClient     *http.Client
	rootUrl        *url.URL
	PrepareRequest func(*http.Request)
}

type simpleResponse struct {
	*http.Response
}

func (res *simpleResponse) Unmarshal(dest interface{}) (err error) {
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}

	return json.Unmarshal(body, dest)
}

func (c *simpleClient) Get(path string) (*simpleResponse, error) {
	url, err := url.Parse(path)
	if err != nil {
		return nil, err
	}

	url = c.rootUrl.ResolveReference(url)

	query := url.Query()
	query.Set("per_page", "100")
	url.RawQuery = query.Encode()

	req, err := http.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	if c.PrepareRequest != nil {
		c.PrepareRequest(req)
	}

	httpResponse, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	res := &simpleResponse{httpResponse}

	return res, nil
}
