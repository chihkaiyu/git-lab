package gitlab

import (
	"fmt"
	"net/url"
	"regexp"

	"gitlab.com/jramsay/git-lab/git"
)

type Remote struct {
	Name     string
	FetchURL *url.URL
	PushURL  *url.URL
}

func (remote *Remote) Project() (p *Project, err error) {
	p, err = NewProjectFromURL(remote.FetchURL)
	return
}

func Remotes() (remotes []Remote, err error) {
	patternRemote := regexp.MustCompile(`(?P<name>.+)\s+(?P<url>.+)\s+\((?P<urlType>push|fetch)\)`)

	rs, err := git.Remotes()
	if err != nil {
		return
	}

	remoteUrls := make(map[string]map[string]string)
	for _, r := range rs {
		if patternRemote.MatchString(r) {
			m := FindStringSubmatchMap(patternRemote, r)

			urlsByType, ok := remoteUrls[m["name"]]
			if !ok {
				urlsByType = make(map[string]string)
				remoteUrls[m["name"]] = urlsByType
			}

			urlsByType[m["urlType"]] = m["url"]
		}
	}

	for name, urls := range remoteUrls {
		r, err := newRemote(name, urls)
		if err == nil {
			remotes = append(remotes, r)
		}
	}

	return
}

func newRemote(name string, urlMap map[string]string) (Remote, error) {
	r := Remote{}

	fetchURL, ferr := git.ParseURL(urlMap["fetch"])
	pushURL, perr := git.ParseURL(urlMap["push"])
	if ferr != nil && perr != nil {
		return r, fmt.Errorf("No valid remote URLs")
	}

	r.Name = name
	if ferr == nil {
		r.FetchURL = fetchURL
	}
	if perr == nil {
		r.PushURL = pushURL
	}

	return r, nil
}
