package gitlab

import (
	g "github.com/jamesramsay/go-gitlab"
)

func StatusGlyph(status string) (s string) {
	switch status {
	case "running":
		s = "⠙"
	case "success":
		s = "✓"
	case "failed":
		s = "✗"
	default:
		s = ""
	}
	return
}

func StatusExitCode(status string) (c int) {
	switch status {
	case "success":
		c = 0
	case "failed":
		c = 1
	case "pending", "running":
		c = 2
	default:
		c = 3
	}
	return
}

type Commit struct {
	Id     string
	Status string
}

func FetchCommit(c *g.Client, p *Project, id string) (*Commit, error) {
	cmt, _, err := c.Commits.GetCommit(p.Path(), id, nil)
	if err != nil {
		return nil, err
	}

	commit := Commit{
		Id:     id,
		Status: string(*cmt.Status),
	}

	return &commit, nil
}

func SummarizeCommitStatuses(statuses []*g.CommitStatus) (s string) {
	var count = map[string]int{}

	s = ""

	for _, j := range statuses {
		if j.Status == "failed" {
			s = j.Status
			return
		}

		count[j.Status]++
	}

	n := len(statuses) - count["manual"]

	switch n {
	case 0:
		return
	case count["pending"]:
		s = "pending"
	case count["cancelled"]:
		s = "cancelled"
	case count["failed"]:
		s = "failed"
	case count["success"]:
		s = "success"
	default:
		s = "running"
	}

	return
}
