package gitlab

import (
	"regexp"
)

type Branch struct {
	Repo *GitRepo
	Name string
}

func (b *Branch) ShortName() string {
	reg := regexp.MustCompile("^refs/(remotes/)?.+?/")
	return reg.ReplaceAllString(b.Name, "")
}
