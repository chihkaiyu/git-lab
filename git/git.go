package git

import (
	"fmt"
	"net/url"
	"os/exec"
	"regexp"
	"strings"
)

func Dir() (string, error) {
	output, err := gitOutput("rev-parse", "--quiet", "--git-dir")
	if err != nil {
		fmt.Println(err)
		return "", fmt.Errorf("Not a git repository")
	}

	return output[0], nil
}

func Remotes() ([]string, error) {
	return gitOutput("remote", "--verbose")
}

func Ref(ref string) (string, error) {
	output, err := gitOutput("rev-parse", "-q", ref)
	if err != nil {
		return "", fmt.Errorf("Unknown revision or path not in the working tree: %s", ref)
	}

	return output[0], nil
}

func CurrentBranch() (string, error) {
	out, err := gitOutput("rev-parse", "--abbrev-ref", "HEAD")
	if err != nil {
		return "", err
	}

	if len(out) == 0 || out[0] == "" || out[0] == "HEAD" {
		return "", nil
	}

	return out[0], nil
}

var protocolRe = regexp.MustCompile("^[a-zA-Z_+-]+://")

func ParseURL(rawURL string) (*url.URL, error) {
	if !protocolRe.MatchString(rawURL) && strings.Contains(rawURL, ":") {
		rawURL = "ssh://" + strings.Replace(rawURL, ":", "/", 1)
	}

	u, err := url.Parse(rawURL)
	if err != nil {
		return nil, err
	}

	return u, nil
}

func gitOutput(input ...string) (output []string, err error) {
	var out []byte
	var args []string

	for _, a := range input {
		args = append(args, a)
	}

	if out, err = exec.Command("git", args...).CombinedOutput(); err != nil {
		return
	}

	for _, line := range strings.Split(string(out), "\n") {
		if strings.TrimSpace(line) != "" {
			output = append(output, string(line))
		}
	}

	return
}
