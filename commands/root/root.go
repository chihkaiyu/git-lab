package root

import (
	"github.com/alecthomas/kingpin"
)

// Cmd is the root command.
var Cmd = kingpin.New("git-lab", "")

// Command registers a command.
var Command = Cmd.Command

func init() {
	Cmd.PreAction(func(ctx *kingpin.ParseContext) error {
		return nil
	})
}
