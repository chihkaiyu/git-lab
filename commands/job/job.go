package job

import (
	"fmt"
	"os"
	"text/tabwriter"

	"gitlab.com/jramsay/git-lab/color"
	"gitlab.com/jramsay/git-lab/commands/root"
	"gitlab.com/jramsay/git-lab/git"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/utils"

	"github.com/alecthomas/kingpin"
	g "github.com/jamesramsay/go-gitlab"
)

func init() {
	var (
		cmd = root.Command("job", "Display CI job information")
	)

	cmd.Action(func(_ *kingpin.ParseContext) error {
		localRepo, err := gitlab.LocalRepo()
		utils.Check(err)

		project, err := localRepo.DefaultProject()
		utils.Check(err)

		sha, err := git.Ref("HEAD")
		utils.Check(err)

		branch, err := localRepo.CurrentBranch()
		utils.Check(err)

		b := branch.ShortName()

		gl := gitlab.NewClient(project.Host)

		optsPipeline := &g.ListProjectPipelinesOptions{Ref: &b, ListOptions: g.ListOptions{PerPage: 100}}
		pipelines, _, err := gl.Pipelines.ListProjectPipelines(project.Path(), optsPipeline)
		utils.Check(err)

		pipelineId := pipelineIdFromSha(pipelines, sha)
		if pipelineId == nil {
			fmt.Println("Could not find matching pipeline")
			return nil
		}

		optsJob := &g.ListJobsOptions{ListOptions: g.ListOptions{PerPage: 100}}
		jobs, _, err := gl.Jobs.ListPipelineJobs(project.Path(), *pipelineId, optsJob)
		utils.Check(err)

		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)

		for _, j := range jobs {
			fmt.Fprintf(w, "%d\t%s\t%s\t\n", j.ID, color.CommitStatus(j.Status, j.Status), j.Name)
		}

		w.Flush()

		return nil
	})
}

func pipelineIdFromSha(pipelines g.PipelineList, sha string) *int {
	for _, p := range pipelines {
		if p.Sha == sha {
			return &p.ID
		}
	}
	return nil
}
