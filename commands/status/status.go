package status

import (
	"fmt"
	"os"
	"text/tabwriter"

	"gitlab.com/jramsay/git-lab/color"
	"gitlab.com/jramsay/git-lab/commands/root"
	"gitlab.com/jramsay/git-lab/git"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/utils"

	"github.com/alecthomas/kingpin"
)

func init() {
	var (
		cmd     = root.Command("status", "Display commit status information")
		short   = cmd.Flag("short", "Output status as a single word.").Short('s').Bool()
		long    = cmd.Flag("long", "Give the output in long-format. This is the default.").Default("true").Bool()
		verbose = cmd.Flag("verbose", "In addition to the status of stages, also show the status of each job and their URL.").Short('v').Bool()
	)

	cmd.Action(func(_ *kingpin.ParseContext) error {
		localRepo, err := gitlab.LocalRepo()
		utils.Check(err)

		project, err := localRepo.DefaultProject()
		utils.Check(err)

		sha, err := git.Ref("HEAD")
		utils.Check(err)

		branch, err := localRepo.CurrentBranch()
		utils.Check(err)
		ref := branch.ShortName()

		gl := gitlab.NewClient(project.Host)

		commit, err := gitlab.FetchCommit(gl, project, sha)
		utils.Check(err)

		status := commit.Status

		if status == "" {
			return nil
		}

		exitCode := gitlab.StatusExitCode(status)

		if *short == true {
			fmt.Printf("%s", color.CommitStatus(status, status))
		} else if *verbose == true {
			fmt.Printf("%s", color.CommitStatus(status, status))

			// TODO: get all statuses
			// for _, s := range statuses {
			// 	fmt.Printf("%s %s\n", s.Status, s.Name)
			// }
		} else if *long == true {
			pipeline, err := gitlab.FetchLastPipelineByCommit(gl, project, sha, ref)
			utils.Check(err)

			pipelineLabel := fmt.Sprintf("Pipeline #%d", pipeline.Id)
			projectLabel := fmt.Sprintf("%s/%s", project.Namespace, project.Name)
			projectBranch := fmt.Sprintf("#%s", ref)

			fmt.Printf("\n%s %s %s\n", color.Pipeline(pipelineLabel), color.Project(projectLabel), color.Ref(projectBranch))
			fmt.Printf("%s\n\n", color.URL(pipeline.URL))
			fmt.Printf("%s\n\n", color.CommitStatus(status, status))

			w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
			for _, stage := range pipeline.Stages {
				fmt.Fprintf(w, "%s\t%s\n", stage.Name, color.Description(stage.StatusDescription()))
				for _, job := range stage.Jobs {
					if job.Status == "failed" {
						fmt.Fprintf(w, "  %s %s\n", color.CommitStatus(job.Status, gitlab.StatusGlyph(job.Status)), job.Name)
					}
				}
			}
			w.Flush()

			fmt.Println()
		}

		os.Exit(exitCode)

		return nil
	})
}
