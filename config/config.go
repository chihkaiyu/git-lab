package config

import (
	"os"
	"os/user"
	"path"

	"github.com/BurntSushi/toml"
)

var (
	Config    config
	xdgConfig = "XDG_CONFIG_DIR"
)

type config struct {
	Servers []server `toml:"server"`
}

type server struct {
	AccessToken string `toml:"personal_access_token"`
	Host        string
}

func LoadConfig() error {
	configFile := configFilePath()

	f, err := os.Open(configFile)
	if err != nil {
		if os.IsNotExist(err) {
			// TODO: first run behaviour - create file
			return nil
		}
		return err
	}
	defer f.Close()

	Config = config{}
	_, err = toml.DecodeReader(f, &Config)
	return err
}

func configFilePath() string {
	// if XDG_CONFIG_DIR is set, depend on that, else fallback on $HOMEDIR/.gitlab
	xdgDir := os.Getenv(xdgConfig)
	if xdgDir != "" {
		return path.Join(xdgDir, "git-lab", "gitlab.toml")
	}

	usr, _ := user.Current()
	return path.Join(usr.HomeDir, ".gitlab")
}

func GetServer(hostname string) *server {
	for _, s := range Config.Servers {
		if hostname == s.Host {
			return &s
		}
	}

	return nil
}
