package config

import (
	"io/ioutil"
	"os"
	"strings"
	"testing"
)

func TestXDGConfigDirDetection(t *testing.T) {
	dir, err := ioutil.TempDir("", t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	// override xdgConfig to make sure we never interfere with the local environment
	xdgConfig = t.Name()
	os.Setenv(xdgConfig, dir)
	defer os.Unsetenv(xdgConfig)

	if !strings.HasPrefix(configFilePath(), dir) {
		t.Fail()
	}
}
