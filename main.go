package main

import (
	"log"

	// Commands
	_ "gitlab.com/jramsay/git-lab/commands/job"
	_ "gitlab.com/jramsay/git-lab/commands/lint"
	_ "gitlab.com/jramsay/git-lab/commands/status"
	_ "gitlab.com/jramsay/git-lab/commands/trace"

	"github.com/alecthomas/kingpin"
	"gitlab.com/jramsay/git-lab/commands/app"
	"gitlab.com/jramsay/git-lab/config"
)

// Cmd is the root command.
var Cmd = kingpin.New("git-lab", "")

// Command registers a command.
var Command = Cmd.Command

func main() {
	if err := config.LoadConfig(); err != nil {
		log.Fatal(err)
	}

	// TODO: version
	app.Run("master")
}
